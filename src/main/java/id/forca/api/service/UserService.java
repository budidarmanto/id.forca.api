package id.forca.api.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import id.forca.api.model.MUser;
import id.forca.api.param.ParamGlobal;
import id.forca.api.repository.MTableSqRepository;
import id.forca.api.repository.MUserRepository;
import id.forca.api.response.ResponseData;
import id.forca.api.util.Utils;

public class UserService {
	
	public ResponseData getUser(ParamGlobal param) {
		String name = param.getName();
		ResponseData resp = null;
    	List<Object> params = new ArrayList<>();
    	String sql = 
    			" SELECT " + 
    			"  * " +
    			" FROM m_user " +
    			" where m_user_id is not null ";
    	if (name != null) {
    		sql+= " and name = ? ";
    		params.add(name);
    	}
    	Utils u = new Utils();
    	List<Object> resultData = new ArrayList<>();
    	resultData = u.getListObjectFromSQL(sql,params);
    	resp = ResponseData.successResponse("Sukses", resultData);
    	return resp;
	}
	
	public ResponseData setUser(MTableSqRepository sq, MUserRepository userRepo, MUser userModel) {
		ResponseData resp = new ResponseData();
		Utils u = new Utils();
		List<Object> resultData = new ArrayList<>();
		
		//Template CRUD
		MUser user = null;
		int maxID = 0;
		if (userModel.getMUserId() > 0) {
			user = userRepo.findOne(userModel.getMUserId());
		} else {
			user = new MUser();
			maxID = u.getMaxID(sq, "m_user");
			user.setMUserId(maxID);
			user.setCreated(new Timestamp(new Date().getTime()));
			user.setCreatedby(new Long(3));
			user.setUsername(userModel.getUsername());
		}
		
		if (user == null) {
			return ResponseData.errorResponse("user tidak ada");
		}
		/////////////////////////////////////////////////////
		
		user.setUpdated(new Timestamp(new Date().getTime()));
		user.setUpdatedby(new Long(3));
		
		user.setFirstName(userModel.getFirstName());
		user.setLastName(userModel.getLastName());
		user.setName(userModel.getName());
		user.setPassword(u.getMD5Hex(userModel.getPassword()));
		user.setPhone(userModel.getPhone());
		
		userRepo.save(user);
    	
		LinkedHashMap<String, Object> mapResult = new LinkedHashMap<String, Object>();
    	mapResult.put("m_user_id", user.getMUserId());
    	resultData.add(mapResult);
    	resp = ResponseData.successResponse("Sukses", resultData);
    	return resp;
	}
}
