package id.forca.api.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import id.forca.api.model.MUserToken;
import id.forca.api.param.ParamGlobal;
import id.forca.api.repository.MTableSqRepository;
import id.forca.api.repository.MUserTokenRepository;
import id.forca.api.response.ResponseData;
import id.forca.api.util.Utils;

public class AuthenticationService {
	
	public ResponseData getToken(MTableSqRepository sq, MUserTokenRepository tr,ParamGlobal param) {
		String username = param.getUsername();
		String password = param.getPassword();
		
		String token = "";
		ResponseData resp = null;
		Utils u = new Utils();
    	List<Object> params = new ArrayList<>();
    	String sqlLogin = 
				" SELECT " + 
				"    m_user_id::int " + 
				"FROM m_user " + 
				"WHERE username = ? " + 
				"AND PASSWORD = ? ";
    	params.add(username);
    	params.add(u.getMD5Hex(password));
    	
    	LinkedHashMap<String, Object> mapLogin = new LinkedHashMap<String, Object>();
    	mapLogin = u.getMapFromSQL(sqlLogin, params);
    	int userID = 0;
    	if (mapLogin.size() > 0) {
    		userID = (int)mapLogin.get("m_user_id");
    		
    		params.clear();
    		String sqlToken = 
        			" SELECT  " + 
        			"    token " + 
        			"FROM m_user_token  " + 
        			"WHERE m_user_id = ? " + 
        			"AND expireddate > now() " +
    				"ORDER BY expireddate DESC " + 
        			"FETCH FIRST 1 ROWS ONLY ";
    		params.add(userID);
    		LinkedHashMap<String, Object> mapToken = new LinkedHashMap<String, Object>();
    		mapToken = u.getMapFromSQL(sqlToken, params);
    		if (mapToken.size() > 0) {
    			token = (String)mapToken.get("token");
    		} else {
    			token = u.getRandomString(50);
    			MUserToken ut = new MUserToken();
    			int maxID = u.getMaxID(sq, "m_user_token");
    			ut.setMUserTokenId(maxID);
    			ut.setCreated(new Timestamp(new Date().getTime()));
    			ut.setCreatedby(new Long(0));
    			ut.setUpdated(new Timestamp(new Date().getTime()));
    			ut.setUpdatedby(new Long(0));
    			
    			ut.setMUserId(userID);
    			ut.setToken(token);
    			
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(new Date());
    			cal.add(Calendar.MONTH, 12);
    			ut.setExpireddate(new Timestamp(cal.getTime().getTime()));
    			
    			tr.save(ut);
    		}
    	}
    	
    	List<Object> resultData = new ArrayList<>();
    	LinkedHashMap<String, Object> mapResult = new LinkedHashMap<String, Object>();
    	mapResult.put("token", token);
    	resultData.add(mapResult);
    	resp = ResponseData.successResponse("Sukses", resultData);
    	return resp;
	}
	
	public ResponseData getUserDataFromToken(ParamGlobal param) {
		Utils u = new Utils();
		return ResponseData.successResponse("Sukses", u.getMapTokenData(param.getToken()));
	}
}
