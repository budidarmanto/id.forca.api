package id.forca.api.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import id.forca.api.model.Product;
import id.forca.api.model.ProductId;
import id.forca.api.param.ParamGlobal;
import id.forca.api.repository.MTableSqRepository;
import id.forca.api.repository.ProductRepository;
import id.forca.api.response.ResponseData;
import id.forca.api.util.Utils;

public class CobaService {
	
	public ResponseData getProduct(ParamGlobal param) {
		String name = param.getName();
		ResponseData resp = null;
    	List<Object> params = new ArrayList<>();
    	String sql = 
    			"SELECT " + 
    			"  * " +
    			" FROM product " +
    			" where product_id is not null ";
    	if (name != null) {
    		sql+= " and name = ? ";
    		params.add(name);
    	}
    	Utils u = new Utils();
    	List<Object> resultData = new ArrayList<>();
    	resultData = u.getListObjectFromSQL(sql,params);
    	resp = ResponseData.successResponse("Sukses", resultData);
    	return resp;
	}
	
	public ResponseData setProduct(ParamGlobal param, ProductRepository pr, MTableSqRepository sq) {
		String name = param.getName();
		String description = param.getDescription();
		
		ResponseData resp = new ResponseData();
		Utils u = new Utils();
		List<Object> resultData = new ArrayList<>();
		int maxID = u.getMaxID(sq, "product");
		ProductId pID = new ProductId();
		pID.setProductId(maxID);
		pID.setName(name);
		pID.setDescription(description);
		Product p = new Product(pID);
		pr.save(p);
    	LinkedHashMap<String, Object> mapResult = new LinkedHashMap<String, Object>();
    	mapResult.put("id", maxID);
    	resultData.add(mapResult);
    	resp = ResponseData.successResponse("Sukses", resultData);
    	return resp;
	}
}
