package id.forca.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.springframework.core.io.ClassPathResource;
import org.springframework.util.DigestUtils;

import id.forca.api.model.MTableSq;
import id.forca.api.repository.MTableSqRepository;

public class Utils {
	public List<Object> getListObjectFromSQL(String sql,List<Object> params) {
        List<Object> listObject = new ArrayList<>(); 
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = openConnection();
        try {
            ps = con.prepareStatement(sql);
            ps = setMultiParam(ps, params);
            ps.setFetchSize(100);
            rs = ps.executeQuery();
            ResultSetMetaData metadata = rs.getMetaData();
            int countColumn = metadata.getColumnCount();
            while (rs.next()) {
            	LinkedHashMap<String, Object> map = new LinkedHashMap<>();
            	for (int a=1; a<=countColumn; a++) {
            		map.put(metadata.getColumnName(a), rs.getObject(metadata.getColumnName(a)));
            	}
            	listObject.add(map);
            }
        } catch (Exception e) {
        	
        } finally {
        	try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            ps = null;
            rs = null;
        }
        return listObject;
    }
	
	public LinkedHashMap<String, Object> getMapFromSQL(String sql,List<Object> params){
		List<Object> listObject = getListObjectFromSQL(sql, params);
		LinkedHashMap<String, Object> map = new LinkedHashMap();
		if (listObject.size() > 0) {
			map = (LinkedHashMap<String, Object>) listObject.get(0);
		}
		return map;
	}
	
	public List<LinkedHashMap<String, Object>> getListMapFromSQL(String sql,List<Object> params){
		List<LinkedHashMap<String, Object>> resultList = new ArrayList<LinkedHashMap<String,Object>>();
		List<Object> listObject = getListObjectFromSQL(sql, params);
		
		for(Object o : listObject) {
			LinkedHashMap<String, Object> map = new LinkedHashMap();
			map = (LinkedHashMap<String, Object>) o;
			resultList.add(map);
		}
		return resultList;
	}
	
	public LinkedHashMap<String, Object> getMapTokenData(String token){
		List<Object> params = new ArrayList<>();
		String sql = 
				" SELECT " + 
				"    b.* " + 
				"FROM m_user_token a " + 
				"INNER JOIN m_user b ON b.m_user_id = a.m_user_id " + 
				"WHERE a.TOKEN = ? ";
		params.add(token);
		return getMapFromSQL(sql, params);
	}
	
	public Connection openConnection () {
		String fileName ="application.properties";
        File file;
        Connection con = null;
        try {
			file = new ClassPathResource(fileName).getFile();
		Properties prop = getPropertiesFromFileName(file.getPath());
        String url = prop.getProperty("spring.datasource.url");
        String user = prop.getProperty("spring.datasource.username");
        String password = prop.getProperty("spring.datasource.password");
        
        con = DriverManager.getConnection(url, user, password);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	
	public Properties getPropertiesFromFileName(String fileName) {
		Properties prop = new Properties();
        try {
        	FileInputStream fis = new FileInputStream(fileName);
            prop.load(fis);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        return prop;
	}
	
	public String getMD5Hex(String str) {
		String result = DigestUtils.md5DigestAsHex(str.getBytes());
		return result;
	}
	
	public int getMaxID(MTableSqRepository sqp, String tableName) {
		List<Object> params = new ArrayList<>();
    	String sql = 
    			" SELECT " + 
    			"  max_id::int " +
    			" FROM m_table_sq " +
    			" where tablename = ? ";
    	params.add(tableName);
    	Integer count = 0;
    	LinkedHashMap<String, Object> map = getMapFromSQL(sql, params);
		count = (Integer)map.get("max_id") == null ? 0 : (Integer)map.get("max_id");
		
		params.clear();
		String sqlMaxID = 
    			" SELECT " + 
    			"  coalesce(max("+tableName+"_id),0)::int + 1 max_id " +
    			" FROM "+tableName;
    	Integer maxID = 0;
    	LinkedHashMap<String, Object> mapID = getMapFromSQL(sqlMaxID, params);
		maxID = (Integer)mapID.get("max_id") == null ? 1 : (Integer)mapID.get("max_id");
		MTableSq tseq = new MTableSq();
		tseq.setTablename(tableName);
		if (count > maxID) {
			maxID = count;
		}
		tseq.setMaxId(maxID);
		sqp.save(tseq);
		
		return (int) tseq.getMaxId();
	}
	
	public String getRandomString(int length) {
		int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = length;
	    Random random = new Random();
	 
	    return random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	}
	
	public final PreparedStatement setMultiParam(PreparedStatement ps, List<Object> params)
            throws SQLException {
        int index = 0;
        for (Object param : params) {
            index++;
            if (param == null)
                ps.setObject(index, null);
            else if (param instanceof String)
                ps.setString(index, (String) param);
            else if (param instanceof Integer)
                ps.setInt(index, ((Integer) param).intValue());
            else if (param instanceof BigDecimal)
                ps.setBigDecimal(index, (BigDecimal) param);
            else if (param instanceof Timestamp)
                ps.setTimestamp(index, (Timestamp) param);
            else if (param instanceof Boolean)
                ps.setString(index, ((Boolean) param).booleanValue() ? "Y" : "N");
            else if (param instanceof byte[])
                ps.setBytes(index, (byte[]) param);
        }
        return ps;
    }
}
