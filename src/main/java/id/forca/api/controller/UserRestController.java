package id.forca.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.forca.api.model.MUser;
import id.forca.api.param.ParamGlobal;
import id.forca.api.repository.MTableSqRepository;
import id.forca.api.repository.MUserRepository;
import id.forca.api.repository.ProductRepository;
import id.forca.api.response.ResponseData;
import id.forca.api.service.CobaService;
import id.forca.api.service.UserService;

@RestController
@RequestMapping("/user")
public class UserRestController {
	Logger log = LoggerFactory.getLogger(UserRestController.class);

	MTableSqRepository sq;
	MUserRepository user;

	public UserRestController( 
			MTableSqRepository sq, 
			MUserRepository user
			) {
		this.sq = sq;
		this.user = user;
	}

	UserService serv = new UserService();
	
	@PostMapping("/getUser")
	public ResponseData getProduct(@RequestParam(required = false, value = "name") String name) {
		ParamGlobal param = new ParamGlobal();
		param.setName(name);
		ResponseData resp = serv.getUser(param);
		return resp;
	}

	@PostMapping("/setUser")
	public ResponseData setProduct(@RequestBody MUser userModel) {
		ResponseData resp = serv.setUser(sq, user, userModel);
		return resp;
	}

}
