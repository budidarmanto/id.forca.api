package id.forca.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.forca.api.param.ParamGlobal;
import id.forca.api.repository.MTableSqRepository;
import id.forca.api.repository.ProductRepository;
import id.forca.api.response.ResponseData;
import id.forca.api.service.CobaService;

@RestController
@RequestMapping("/tesrest")
public class CobaRestController {
	Logger log = LoggerFactory.getLogger(CobaRestController.class);

	ProductRepository pr;
	MTableSqRepository sq;

	public CobaRestController(ProductRepository pr, MTableSqRepository sq) {
		this.pr = pr;
		this.sq = sq;
	}

	CobaService serv = new CobaService();
	
	@PostMapping("/getProduct")
	public ResponseData getProduct(@RequestParam(required = false, value = "name") String name) {
		ParamGlobal param = new ParamGlobal();
		param.setName(name);
		ResponseData resp = serv.getProduct(param);
		return resp;
	}

	@PostMapping("/setProduct")
	public ResponseData setProduct(@RequestParam(value = "name") String name,
			@RequestParam(value = "description") String description) {
		ParamGlobal param = new ParamGlobal();
		param.setName(name);
		param.setDescription(description);

		ResponseData resp = serv.setProduct(param, pr, sq);
		return resp;
	}

}
