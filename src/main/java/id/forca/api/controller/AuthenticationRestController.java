package id.forca.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.forca.api.param.ParamGlobal;
import id.forca.api.repository.MTableSqRepository;
import id.forca.api.repository.MUserRepository;
import id.forca.api.repository.MUserTokenRepository;
import id.forca.api.response.ResponseData;
import id.forca.api.service.AuthenticationService;

@RestController
@RequestMapping("/authentication")
public class AuthenticationRestController {
	Logger log = LoggerFactory.getLogger(AuthenticationRestController.class);

	MTableSqRepository sq;
	MUserRepository user;
	MUserTokenRepository userToken;

	public AuthenticationRestController(MTableSqRepository sq, MUserRepository user, MUserTokenRepository userToken) {
		this.sq = sq;
		this.user = user;
		this.userToken = userToken;
	}

	AuthenticationService serv = new AuthenticationService();

	@PostMapping("/getToken")
	public ResponseData getToken(@RequestParam(required = true, value = "username") String username,
			@RequestParam(required = true, value = "password") String password) {
		ParamGlobal param = new ParamGlobal();
		param.setUsername(username);
		param.setPassword(password);
		ResponseData resp = serv.getToken(sq, userToken, param);
		return resp;
	}
	
	@PostMapping("/getUserDataFromToken")
	public ResponseData getUserDataFromToken(@RequestParam(required = true, value = "token") String token) {
		ParamGlobal param = new ParamGlobal();
		param.setToken(token);
		ResponseData resp = serv.getUserDataFromToken(param);
		return resp;
	}

}
