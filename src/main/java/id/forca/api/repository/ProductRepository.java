package id.forca.api.repository;

import org.springframework.data.repository.CrudRepository;

import id.forca.api.model.Product;
import id.forca.api.model.ProductId;

public interface ProductRepository extends CrudRepository<Product, ProductId> {

}
