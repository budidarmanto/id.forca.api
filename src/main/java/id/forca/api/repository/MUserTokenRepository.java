package id.forca.api.repository;

import org.springframework.data.repository.CrudRepository;

import id.forca.api.model.MUserToken;

public interface MUserTokenRepository extends CrudRepository<MUserToken, Long> {

}
