package id.forca.api.repository;

import org.springframework.data.repository.CrudRepository;

import id.forca.api.model.MTableSq;

public interface MTableSqRepository extends CrudRepository<MTableSq, String> {

}
