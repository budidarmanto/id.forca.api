package id.forca.api.repository;

import org.springframework.data.repository.CrudRepository;

import id.forca.api.model.MTableSq;
import id.forca.api.model.MUser;

public interface MUserRepository extends CrudRepository<MUser, Long> {

}
